#include <iostream>
#include <vector>
#include <algorithm>

int main(){

  std::vector<int> myInt;

  myInt.assign({1,2,42,4,5,6});

  int largest;

  std::for_each(myInt.begin(), myInt.end(), [&](int a){
    if(a == myInt[0]){
      largest = a;
    }
    if(a > largest){
      largest = a;
    }
  });
  
  std::cout << largest;


  return 0;
}