#include <iostream>
int main()
{ /* PROGRAM TO PRINT OUT SPACE RESERVED FOR VARIABLES */
	char c;  
	short s;  
	int i;  
	unsigned int ui;  
	unsigned long int ul; 
	float f;
	double d;  
	long double ld;  
	std::cout << std::endl << "The storage space for each variable type is: " << std::endl;
	std::cout << std::endl << "char: \t\t\t bits " << sizeof(c)*8 << std::endl; 
	std::cout << std::endl << "short: \t\t\t bits " << sizeof(s)*8 << std::endl;
	std::cout << std::endl << "int: \t\t\t bits " << sizeof(i)*8 << std::endl;
	std::cout << std::endl << "unsigned int: \t\t bits " << sizeof(ui)*8 << std::endl;
	std::cout << std::endl << "unsigned long int: \t bits " << sizeof(ul)*8 << std::endl;
	std::cout << std::endl << "float: \t\t\t bits " << sizeof(f)*8 << std::endl;
	std::cout << std::endl << "double: \t\t bits " << sizeof(d)*8 << std::endl;
	std::cout << std::endl << "long double: \t\t bits " << sizeof(ld)*8 << std::endl;
}