#include <iostream>
#include <limits>

int main(){

  char myChar;
  int myInt;
  float myFloat;
  std::cout << "Input a single character,  followed by : ";
  std::cin >> myChar;
  std::cout << "\nInput an integer, followed by : ";
  while(!(std::cin >> myInt)){
    std::cout << "!!Not an integer.\n";
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Input an integer, followed by : ";
  }
  std::cout << "\nInput a float, followed by : ";
  while(!(std::cin >> myFloat)){
    std::cout << "!!Not a float,\n";
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Input a float, followed by : ";
  }
  std::cout << "\nThe character " << myChar << " when cast to an int gives va";
  std::cout << "lue " << static_cast<int>(myChar) << "\n";
  std::cout << "The integer " << myInt << " when cast to a float gives a va";
  std::cout << "lue " << static_cast<float>(myInt) << "\n";
  std::cout << "The float " << myFloat << " when cast to a integer gives the ";
  std::cout << "value " << static_cast<int>(myFloat) << "\n";

}