# Description
This is a git repository containing my journey through c++ and some of its core libraries mainly STL.
# Organization
Each folder contains a specific topic that is exposed and documented 
for example the stl folder references the stl topic and inside it there will be folder such as 
container which goes through the different containers in the stl and so on.
# Topics 
There are multiple md files in this repo that have the name of a topic and describe the location of 
source code in which that specific topic is covered whithin a particular folder.

