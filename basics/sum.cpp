#include <iostream>

int main(){
    // variable declarations note that you have to define the variable type
    int firstVariable = 1;
    int secondVariable = 2;
    int thirdVariable = secondVariable + firstVariable;   
    std::cout << thirdVariable;
}